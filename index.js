const fs = require('fs');
const path = require('path');

const LOG_FILE = '/tmp/.esm-resolver.log';
const EXTENSIONS = ['jsx', 'js', 'ts', 'tsx', 'json'];

const exists = (itemPath) => {
  try {
    fs.lstatSync(itemPath);
    return itemPath;
  } catch (_err) {
    return false;
  }
};

exports.resolve = (source, file, config = {}) => {
  if (!source.startsWith('#')) return { found: false };

  const { extensions = EXTENSIONS, debug = false } = config;

  const packageJsonDir = config.paths
    .map((p) => path.resolve(p))
    .find((dir) => file.startsWith(dir));

  const packageJson = JSON.parse(
    fs.readFileSync(`${packageJsonDir}/package.json`).toString()
  );

  const { imports = {} } = packageJson;

  if (imports[source]) {
    const qPath = path.resolve(packageJsonDir, imports[source]);
    if (!exists(qPath)) return { found: false };
    return {
      found: true,
      path: qPath,
    };
  }

  const keys = Object.keys(imports).filter((key) => key.endsWith('*'));

  const debugMap = {};

  // eslint-disable-next-line
  for (const key of keys) {
    const prefix = key.split('/').slice(0, -1).join('/');
    const computedSource = source.replace(prefix, '.');

    const aliasValue = imports[key];
    const knownPath = aliasValue.split('/').slice(0, -1).join('/');

    const importPath = path.resolve(packageJsonDir, knownPath, computedSource);

    let qImportPath = exists(importPath);

    // eslint-disable-next-line
    for (const ext of extensions) {
      if (qImportPath) break;
      qImportPath = exists(`${importPath}.${ext}`);
    }

    if (qImportPath) {
      const result = { found: true, path: qImportPath };
      if (!debug) return result;
      debug.result = result;
    }

    debugMap[prefix] = {
      source,
      file,
      config,

      packageJsonDir,
      prefix,
      key,
      computedSource,
      aliasValue,
      knownPath,
      importPath,
      qImportPath,
    };
  }

  fs.writeFileSync(
    LOG_FILE,
    JSON.stringify(
      {
        imports,
        debugMap,
      },
      null,
      2
    )
  );

  if (debug && debug.result) return debug.result;

  return { found: false };
};

exports.interfaceVersion = 2;
